# Job Vacancy SQL
![PT Tatalogam Lestari](https://image-service-cdn.seek.com.au/c942ff6e9648906e032dcf692114fe329c0d9d88/a868bcb8fbb284f4e8301904535744d488ea93c1)

## Data Analyst or Software di [PT. Tatalogam Lestari](https://tatalogam.com/) - Jakarta Barat

## Gaji
IDR 5.000.000 - IDR 7.000.000

## Deskripsi Pekerjaan :
- Bertugas Bermitra erat dengan Tim lain untuk membentuk strategi membangun data kami yang berkembang
- Bertanggung jawab Merancang dan mengembangkan data termasuk menentukan pemetaan sumber-ke-target, aturan bisnis, dan transformasi data lain yang diperlukan
- Bertugas optimalkan pemrosesan integrasi data dan desain basis data
- Bertugas mengembangkan dasbor dan laporan menggunakan alat pelaporan BI seperti Tableau, Metabase, dan Ind House App
- Bertanggung jawab terjemahkan tugas analitik kompleks menjadi keluaran yang mudah digunakan untuk memandu keputusan berdasarkan data dengan berbagai pemangku kepentingan
- Bertugas merancang arsitektur dan subsistem baru
- Bertugas mengidentifikasi dan selesaikan masalah teknis pada proses bisnis di dalam sistem
- Bertugas menyusun dan menganalisis data, proses, dan kode untuk memecahkan masalah dan mengidentifikasi area untuk perbaikan
- Bertugas mengembangkan ide untuk aplikasi baru, dan fitur dengan memantau perkembangan dan trend industri
- Bertugas mengintegrasikan aplikasi proyek dengan API backend yang ada

## Kualifikasi
- Usia maksimal 35 tahun
- Pendidikan minimal S1/SMK Teknik Informatika/Management/Sistem Informasi/Teknik Industri/Statistik/Akuntansi
- Diutamakan pengalaman minimal 1 tahun dibidang Data Analytics/Data Engineering/Website / Application Development
- Kemampuan untuk bekerja secara mandiri atau dengan kelompok
- Kemampuan untuk Javascript dan Javascript Framework
- Memahami pengetahuan dan pengalaman SQL seperti : query authoring (SQL),postgreSQL, SQL Server, dan mySQL
- Kemampuan untuk melakukan analisis akar masalah pada data dan proses internal dan eksternal untuk menjawab pertanyaan bisnis tertentu dan mengidentifikasi peluang untuk perbaikan
- Kemampuan untuk Pengalaman mendukung dan bekerja dengan tim lintas fungsi dalam lingkungan yang dinamis
- Memahami Mengembangkan dasbor dan laporan menggunakan alat pelaporan BI seperti Tableau, Metabase, dll
- Memiliki kemampuan mendalam tentang keseluruhan proses pengembangan web (desain, pengembangan, dan penerapan)

## Informasi Tambahan
### Tingkat Pekerjaan
Pegawai (non-manajemen & non-supervisor)
### Kualifikasi
Sarjana (S1)
### Pengalaman Kerja
1 tahun
### Jenis Pekerjaan
Penuh Waktu
### Spesialisasi Pekerjaan
Komputer/Teknologi Informasi, IT-Perangkat Lunak

[Link Jobstreet](https://www.jobstreet.co.id/id/job/data-analyst-or-software-4360503?jobId=jobstreet-id-job-4360503&sectionRank=10&token=0~c2482179-dd0e-42ca-9e66-cde51f2bc8ea&searchPath=%2Fid%2FData-Engineer-SQL-jobs%3Fsalary%3D5000000%26salary-max%3D2147483647&fr=SRP%20View%20In%20New%20Tab)
